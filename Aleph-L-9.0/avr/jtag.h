#pragma once

/** \file **********************************************************************
 *
 * steruje interfejsem jtag
 *
 * aby uniknąć niezamierzonego wyłączenia lub włączenia interfejsu JTAG,
 * podczas zmiany bitu JTD należy przestrzegać sekwencji czasowej:
 * oprogramowanie musi zapisać bit JTD do żądanej wartości dwa razy
 * w ciągu czterech cykli, aby zmienić jego wartość.
 *
 ******************************************************************************/

#include <avr/io.h>
#include <avr/iom32.h>
#include <common/gcc_attributes.h>
#include <stdint.h>

/*
 * wyłącza interfejs jtag
 *
 * wyłączenie interfejsu umożliwia wykorzystanie wszystkich pinów portu
 * procesora na który jest wyprowadzony jtag
 */
__gcc_static_inline void avr_jtag_interface_disable(void)
{
	MCUCSR |= (1U << JTD);
	MCUCSR |= (1U << JTD);
}

/*
 * włącza interfejs jtag
 *
 * po resecie procesora bit JTD ma wartość 0, interfejs JTAG jest włączony.
 */
__gcc_static_inline void avr_jtag_interface_enable(void)
{
	MCUCSR &= (uint8_t) (~(1U << JTD));
	MCUCSR &= (uint8_t) (~(1U << JTD));
}

