#include "../device.h"

#include <avr/builtins.h>
#include <avr/pgmspace.h>
#include <common/encoder_event_detector.h>
#include <common/finite_state_machine.h>
#include <common/gcc_attributes.h>
#include <common/key_press_type_detector.h>
#include <common/ring_buffer.h>
#include <common/software_timer.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "../../avr/jtag.h"
#include "../../avr/port.h"
#include "../../driver/attenuator.h"
#include "../../driver/encoder.h"
#include "../../driver/indicator.h"
#include "../../driver/input.h"
#include "../../driver/key.h"
#include "../../driver/settings.h"
#include "../../driver/signal.h"
#include "../../driver/supply.h"
#include "../../hal/encoder.h"
#include "../../hal/input.h"
#include "../../hal/signal.h"
#include "../../timebase/timebase.h"

/*
 * 24 krokowa, logarytmiczna krzywa tłumienia wg manuala Aleph-L
 * zmodyfikowane kroki 1,2 (w oryginale 62 i 53)
 */
static uint8_t const PROGMEM volume_damping[] = {
		63U, 57U, 51U, 46U, 41U, 37U, 34U, 32U, 30U, 28U,
		26U, 24U, 22U, 20U, 18U, 16U, 14U, 12U, 10U, 8U,
		6U, 4U, 2U, 0U };

enum {
	DAMPING_STEP_MIN = 0U,
	DAMPING_STEP_MAX = ((sizeof(volume_damping) / sizeof(volume_damping[0])) - 1U),
	DAMPING_STEP_DEFAULT = (DAMPING_STEP_MAX - DAMPING_STEP_MIN) / 2U
};

static uint16_t const DEVICE_STARTUP_DELAY_MS = 2500U;

static uint16_t const INDICATOR_ON_TIME_MS = 800U;
static uint16_t const INDICATOR_OFF_TIME_MS = 200U;

static uint16_t SIGNAL_MUTE_ON_TIME_MS = 200U;
static uint16_t SIGNAL_MUTE_OFF_TIME_MS = 800U;

static uint8_t const KEY_DEBOUNCING_TIME_MS = 50U;
static uint16_t const KEY_DOUBLEPRESS_TIME_MS = 600U;
static uint16_t const KEY_LONGSTART_TIME_MS = 1000U;
static uint16_t const KEY_LONGREPEAT_TIME_MS = 250U;

/* musi być potęgą 2^n */
enum {
	EVENT_QUEUE_SIZE = 8U
};

enum device_event {
	DEVICE_NOTHING_EV = FSM_FIRST_USER_EV,
	DEVICE_TIMEOUT_EV,
	DEVICE_SUPPLY_EV,
	DEVICE_SOURCE_MUTE_EV,
	DEVICE_SOURCE_NEXT_EV,
	DEVICE_SOURCE_PREVIOUS_EV,
	DEVICE_VOLUME_INCREASE_EV,
	DEVICE_VOLUME_DECREASE_EV
};

static struct device {
	fsm_st fsm;
	fsm_state_st initial_state;
	fsm_state_st standby_state;
	fsm_state_st working_state;
	ring_buffer_st event_queue;
	uint8_t queue_container[EVENT_QUEUE_SIZE];
	settings_st settings;
	software_timer_st startup_timer;
} device = { 0 };

static settings_st const settings_default PROGMEM = {.damping_step = DAMPING_STEP_MIN, .input_line = INPUT_0, .input_mute = MUTE_ON};

static void initial_handler(fsm_st *me, fsm_msg_st const *msg);
static void standby_handler(fsm_st *me, fsm_msg_st const *msg);
static void working_handler(fsm_st *me, fsm_msg_st const *msg);

static void msg_init(void);
static void msg_send_event(uint8_t event);
static uint8_t msg_read_event(void);

static void settings_restore(void);
static void settings_store(void);

static void encoder_volume_handler(encoder_event_et event, void *arg);
static void encoder_input_handler(encoder_event_et event, void *arg);
static void key_handler(key_press_event_et event, void *arg);
static void startup_handler(void *arg);

static void tick_time_handler(void *arg);

void device_init(void)
{
	avr_jtag_interface_disable();
	avr_port_initial_settings();

	drv_supply_init();
	drv_indicator_init();
	drv_input_init();
	drv_signal_indicator_init();
	drv_attenuator_init();
	drv_encoder_init();
	drv_key_init(KEY_DEBOUNCING_TIME_MS, KEY_DOUBLEPRESS_TIME_MS,
			KEY_LONGSTART_TIME_MS, KEY_LONGREPEAT_TIME_MS);
	drv_settings_init();

	msg_init();
	software_timer_init(&(device.startup_timer));
	settings_restore();

	timebase_start(tick_time_handler, NULL);

	fsm_state_ctor(&(device.initial_state), initial_handler);
	fsm_state_ctor(&(device.standby_state), standby_handler);
	fsm_state_ctor(&(device.working_state), working_handler);

	fsm_ctor(&(device.fsm), &(device.initial_state));
	fsm_on_start(&(device.fsm));

	__builtin_avr_sei();
}

void device_handle_events(void)
{
	uint8_t event = msg_read_event();

	if (DEVICE_NOTHING_EV != event) {
		fsm_on_event(&(device.fsm), &((fsm_msg_st ) { .event = event } ));
	}
}

void initial_handler(fsm_st *me, fsm_msg_st const *msg)
{
	switch (msg->event) {
	case FSM_START_EV:
		software_timer_start(&(device.startup_timer), DEVICE_STARTUP_DELAY_MS, startup_handler, NULL);
		drv_signal_indicator_on(SIGNAL_ALL);
		break;

	case FSM_EXIT_EV:
		drv_encoder_handler(ENCODER_INPUT, encoder_input_handler, NULL);
		drv_encoder_handler(ENCODER_VOLUME, encoder_volume_handler, NULL);
		drv_key_handler(key_handler, NULL);
		break;

	case DEVICE_TIMEOUT_EV:
		fsm_state_transition(me, &(device.standby_state));
		break;

	default:
		;
		break;
	}
}

void standby_handler(fsm_st *me, fsm_msg_st const *msg)
{
	switch (msg->event) {
	case FSM_ENTRY_EV:
		drv_supply_off();
		drv_indicator_blink(INDICATOR_ON_TIME_MS, INDICATOR_OFF_TIME_MS);
		drv_input_set((device.settings).input_line, MUTE_ON);
		drv_signal_indicator_off();
		drv_attenuator_damping_set(pgm_read_byte(&volume_damping[DAMPING_STEP_MIN]));
		break;

	case DEVICE_SUPPLY_EV:
		fsm_state_transition(me, &(device.working_state));
		break;

	default:
		;
		break;
	}
}

void working_handler(fsm_st *me, fsm_msg_st const *msg)
{
	static bool input_signal_refresh = false;
	static bool volume_refresh = false;

	switch (msg->event) {
	case FSM_ENTRY_EV:
		drv_supply_on();
		drv_indicator_on();
		input_signal_refresh = true;
		volume_refresh = true;
		break;

	case FSM_EXIT_EV:
		settings_store();
		break;

	case DEVICE_SUPPLY_EV:
		fsm_state_transition(me, &(device.standby_state));
		break;

	case DEVICE_VOLUME_DECREASE_EV:
		if (DAMPING_STEP_MIN  < (device.settings).damping_step) {
			(device.settings).damping_step = (uint8_t) ((device.settings).damping_step - 1U);
			volume_refresh = true;
		}

		break;

	case DEVICE_VOLUME_INCREASE_EV:
		if (DAMPING_STEP_MAX > (device.settings).damping_step) {
			(device.settings).damping_step = (uint8_t) ((device.settings).damping_step + 1U);
			volume_refresh = true;
		}

		break;

	case DEVICE_SOURCE_NEXT_EV:
		if (INPUT_3 > (device.settings).input_line) {
			(device.settings).input_line = (uint8_t) ((device.settings).input_line + 1U);
			input_signal_refresh = true;
		}

		break;

	case DEVICE_SOURCE_PREVIOUS_EV:
		if (INPUT_0 < (device.settings).input_line) {
			(device.settings).input_line = (uint8_t) ((device.settings).input_line - 1U);
			input_signal_refresh = true;
		}

		break;

	case DEVICE_SOURCE_MUTE_EV:

		(device.settings).input_mute = !((device.settings).input_mute);
		input_signal_refresh = true;
		break;

	default:
		;
		break;
	}

	if (true == volume_refresh) {
		drv_attenuator_damping_set(pgm_read_byte(&volume_damping[(device.settings).damping_step]));
		volume_refresh = false;
	}

	if (true == input_signal_refresh) {
		drv_input_set((device.settings).input_line, (device.settings).input_mute);
		drv_signal_indicator_on((signal_et)((device.settings).input_line));

		if (MUTE_ON == (device.settings).input_mute) {
			drv_signal_indicator_blink(SIGNAL_MUTE_ON_TIME_MS, SIGNAL_MUTE_OFF_TIME_MS);
		}

		input_signal_refresh = false;
	}
}

void msg_init(void)
{
	ring_buffer_init(&(device.event_queue), device.queue_container, EVENT_QUEUE_SIZE);
}

void msg_send_event(uint8_t event)
{
	ring_buffer_reject_put(&(device.event_queue), event);
}

uint8_t msg_read_event(void)
{
	uint8_t event = DEVICE_NOTHING_EV;

	ring_buffer_get(&(device.event_queue), &event);

	return (event);
}

void settings_restore(void)
{
	bool read_ok = drv_settings_read(&(device.settings));

	if (false == read_ok) {
		memcpy_P(&(device.settings), &settings_default, sizeof(settings_st));
	}
}

void settings_store(void)
{
	drv_settings_write(&(device.settings));
}

void encoder_volume_handler(encoder_event_et event, void *arg __gcc_maybe_unused)
{
	switch (event) {
	case ENCODER_CW_EVENT:
		msg_send_event(DEVICE_VOLUME_INCREASE_EV);
		break;

	case ENCODER_CCW_EVENT:
		msg_send_event(DEVICE_VOLUME_DECREASE_EV);
		break;

	default:
		;
		break;
	}
}

void encoder_input_handler(encoder_event_et event, void *arg __gcc_maybe_unused)
{
	switch (event) {
	case ENCODER_CW_EVENT:
		msg_send_event(DEVICE_SOURCE_NEXT_EV);
		break;

	case ENCODER_CCW_EVENT:
		msg_send_event(DEVICE_SOURCE_PREVIOUS_EV);
		break;

	default:
		;
		break;
	}
}

void key_handler(key_press_event_et event, void *arg __gcc_maybe_unused)
{
	switch (event) {
	case KEY_SHORT_PRESSED_EV:
		msg_send_event(DEVICE_SUPPLY_EV);
		break;

	case KEY_DOUBLE_PRESSED_EV:
		msg_send_event(DEVICE_SOURCE_MUTE_EV);
		break;

	case KEY_LONGPRESS_START_EV:
		msg_send_event(DEVICE_SOURCE_MUTE_EV);
		break;

	default:
		;
		break;
	}
}

void startup_handler(void *arg __gcc_maybe_unused)
{
	msg_send_event(DEVICE_TIMEOUT_EV);
}

void tick_time_handler(void *arg __gcc_maybe_unused)
{
	drv_indicator_on_tick_time();
	drv_input_on_tick_time();
	drv_signal_indicator_on_tick_time();
	drv_attenuator_on_tick_time();
	drv_encoder_on_tick_time();
	drv_key_on_tick_time();
	software_timer_on_tick_time(&(device.startup_timer));
}

