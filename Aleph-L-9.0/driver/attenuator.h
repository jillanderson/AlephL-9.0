#pragma once

/** \file **************************************************************************************************************
 *
 * sterowanie tłumikiem sygnału
 *
 **********************************************************************************************************************/

#include <stdbool.h>
#include <stdint.h>

/*
 * inicjalizuje tłumik sygnału
 * ustawia maksymalne tłumienie (włącza wszystkie tłumiki)
 */
void drv_attenuator_init(void);

/*
 * ustawia zadaną wartość tłumienia
 *
 * @value	wartość tłumienia w zakresie od 0dB do 63dB
 */
void drv_attenuator_damping_set(uint8_t value);

/*
 * odlicza jednostki czasu
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void drv_attenuator_on_tick_time(void);

