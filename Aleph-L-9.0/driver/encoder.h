#pragma once

/** \file **************************************************************************************************************
 *
 * obsługa enkoderów: regulacji głośności i przełączania wejść
 *
 **********************************************************************************************************************/

#include "../hal/encoder.h"

#include <common/encoder_event_detector.h>

/*
 * inicjalizuje obsługę enkoderów
 */
void drv_encoder_init(void);

/*
 * rejestruje funkcję obsługi zdarzeń encodera
 *
 * @encoder	identyfikator enkodera
 * @handler	funkcja obsługująca zdarzenia od enkodera
 * @arg		wskaźnik do argumentu funkcji zwrotnej
 */
void drv_encoder_handler(encoder_et encoder, encoder_event_handler_ft *handler, void *arg);

/*
 * uaktualnia stan systemu wykrywania zdarzeń od enkoderów
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void drv_encoder_on_tick_time(void);

