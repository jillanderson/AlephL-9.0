#pragma once

/** \file **************************************************************************************************************
 *
 * sterowanie przełączaniem linii wejściowych
 *
 **********************************************************************************************************************/

#include "../hal/input.h"

#include <stdbool.h>

typedef enum {
	MUTE_OFF = false,
	MUTE_ON = true
} mute_et;

/*
 * inicjalizuje przełącznik wejść
 * odłącza  wszystkie wejścia, włącza wyciszenie
 */
void drv_input_init(void);

/*
 * ustawia linię wejściową i stan wyciszenia
 *
 * @input	zadana linia
 * @mute	stan wyciszenia
 */
void drv_input_set(input_et input, mute_et mute);

/*
 * odlicza jednostki czasu
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void drv_input_on_tick_time(void);

