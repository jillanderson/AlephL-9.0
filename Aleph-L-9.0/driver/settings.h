#pragma once

/** \file **********************************************************************
 *
 * wear leveling EEPROM
 *
 ******************************************************************************/

#include <stdbool.h>
#include <stdint.h>

typedef struct {
	uint8_t damping_step;
	uint8_t input_line;
	bool input_mute;
} settings_st;

/*
 * inicjuje zapis ustawień w pamięci EEPROM
 */
void drv_settings_init(void);

/*
 * odczytuje ustawienia
 *
 * @par	wskaźnik do ustawień
 * @ret	true - ustawienia odczytane prawidłowo
 * 	false - ustawienia odczytane nieprawidłowo
 */
bool drv_settings_read(settings_st *settings);

/*
 * zapisuje ustawienia
 *
 * @par	wskaźnik do ustawień
 */
void drv_settings_write(settings_st *settings);

