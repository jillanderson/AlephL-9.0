#pragma once

/** \file **********************************************************************
 *
 * sterowanie wskaźnikami optycznymi linii wejściowych
 *
 ******************************************************************************/

#include "../hal/signal.h"

#include <stdint.h>

/*
 * inicjuje sterowanie wskaźnikami linii wejściowych
 * ustawia jako aktywne i wyłącza wszystkie wskaźniki
 */
void drv_signal_indicator_init(void);

/*
 * ustawia zadany wskaźnik linii wejściowej jako aktywny, włącza wskaźnik
 * zatrzymuje timer
 *
 * @signal	zadany wskaźnik. Jeśli signal >= SIGNAL_QTY wszystkie wskaźniki
 */
void drv_signal_indicator_on(signal_et signal);

/*
 * wyłącza aktywny wskaźnik
 */
void drv_signal_indicator_off(void);

/*
 * włącza aktywny wskaźnik w tryb błyskania z zadanymi czasami włączenia
 * i wyłączenia wskaźnika
 *
 * @on_time	czas włączenia
 * @off_time	czas wyłączenia
 */
void drv_signal_indicator_blink(uint16_t on_time_ms, uint16_t off_time_ms);

/*
 * odlicza jednostki czasu
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void drv_signal_indicator_on_tick_time(void);

