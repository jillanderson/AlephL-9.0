#include "../../hal/attenuator.h"
#include "../attenuator.h"

#include <stdint.h>

/* zalecany czas debouncingu przekaźników miniaturowych wynosi 5-15ms */
static uint8_t const RELAY_BOUNCE_TIME_MS = 15U;

static struct {
	volatile uint16_t time_counter;
	volatile enum {
		COUNTER_STOPPED,
		COUNTER_COUNTING
	} counter_state;
	volatile enum {
		STAGE_DIFF,
		STAGE_VALUE
	} set_stage;
	uint8_t next;
	volatile uint8_t tmp;
	volatile uint8_t diff;
} attenuator = { 0 };

void drv_attenuator_init(void)
{
	hal_attenuator_init();
}

void drv_attenuator_damping_set(uint8_t value)
{
	attenuator.counter_state = COUNTER_STOPPED;

	uint8_t const dv_max = ((1U << ATTENUATOR_QTY) - 1U);

	attenuator.next = value;

	if (dv_max < value) {
		attenuator.next = dv_max;
	}

	attenuator.tmp = (uint8_t) (((hal_attenuator_is_on(ATTENUATOR_32DB)) << ATTENUATOR_32DB)
			| ((hal_attenuator_is_on(ATTENUATOR_16DB)) << ATTENUATOR_16DB)
			| ((hal_attenuator_is_on(ATTENUATOR_8DB)) << ATTENUATOR_8DB)
			| ((hal_attenuator_is_on(ATTENUATOR_4DB)) << ATTENUATOR_4DB)
			| ((hal_attenuator_is_on(ATTENUATOR_2DB)) << ATTENUATOR_2DB)
			| ((hal_attenuator_is_on(ATTENUATOR_1DB)) << ATTENUATOR_1DB));

	attenuator.diff = attenuator.next | attenuator.tmp;

	attenuator.set_stage = STAGE_DIFF;
	attenuator.time_counter = 0U;
	attenuator.counter_state = COUNTER_COUNTING;
}

void drv_attenuator_on_tick_time(void)
{
	if (COUNTER_COUNTING == attenuator.counter_state) {
		if (0U < attenuator.time_counter) {
			attenuator.time_counter--;
		}

		if (0U == attenuator.time_counter) {
			attenuator.time_counter = RELAY_BOUNCE_TIME_MS;

			switch (attenuator.set_stage) {
			case STAGE_DIFF:
				for (uint8_t i = ATTENUATOR_QTY; 0U < i; i--) {

					if ((attenuator.tmp & (1U << (i - 1U))) != (attenuator.diff & (1U << (i - 1U)))) {
						attenuator.tmp |= (uint8_t) (1U << (i - 1U));
						hal_attenuator_on((i - 1U));
						break;
					}
				}

				if ((attenuator.tmp) == (attenuator.diff)) {
					(attenuator.set_stage) = STAGE_VALUE;
				}

				break;

			case STAGE_VALUE:
				for (uint8_t i = 0U; ATTENUATOR_QTY > i; i++) {

					if (((1U << i) == (attenuator.tmp & (1U << i))) && (0U == (attenuator.next & (1U << i)))) {
						attenuator.tmp &= (uint8_t) (~(1U << i));
						hal_attenuator_off(i);
						break;
					}
				}

				if ((attenuator.tmp) == (attenuator.next)) {
					attenuator.counter_state = COUNTER_STOPPED;
				}

				break;

			default:
				;
				break;
			}
		}
	}
}

