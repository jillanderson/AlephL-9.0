#include "../../hal/indicator.h"
#include "../indicator.h"

#include <stdbool.h>
#include <stdint.h>

static struct {
	uint16_t on_time;
	uint16_t off_time;
	volatile uint16_t time_counter;
	volatile enum {
		COUNTER_STOPPED,
		COUNTER_COUNTING
	} counter_state;
} indicator = { 0 };

static void indicator_switch_state(void);

void drv_indicator_init(void)
{
	hal_indicator_init();
}

void drv_indicator_on(void)
{
	hal_indicator_on();
	indicator.counter_state = COUNTER_STOPPED;
}

void drv_indicator_off(void)
{
	hal_indicator_off();
	indicator.counter_state = COUNTER_STOPPED;
}

void drv_indicator_blink(uint16_t on_time_ms, uint16_t off_time_ms)
{
	indicator.on_time = on_time_ms;
	indicator.off_time = off_time_ms;
	indicator_switch_state();
	indicator.counter_state = COUNTER_COUNTING;
}

indicator_status_et drv_indicator_is_on(void)
{
	return (hal_indicator_is_on());
}

void drv_indicator_on_tick_time(void)
{
	if (COUNTER_COUNTING == indicator.counter_state) {

		if (0U < indicator.time_counter) {
			indicator.time_counter--;
		}

		if (0U == indicator.time_counter) {
			indicator_switch_state();
		}
	}
}

void indicator_switch_state(void)
{
	hal_indicator_toggle();
	indicator.time_counter = (true == hal_indicator_is_on()) ?
					indicator.on_time : indicator.off_time;
}

