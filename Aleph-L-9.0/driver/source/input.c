#include "../../hal/input.h"
#include "../input.h"

#include <stdint.h>

#include "../../hal/mute.h"

/* zalecany czas debouncingu przekaźników miniaturowych wynosi 5-15ms */
static uint8_t const RELAY_BOUNCE_TIME_MS = 15U;

static struct {
	volatile uint16_t time_counter;
	volatile enum {
		COUNTER_STOPPED,
		COUNTER_COUNTING
	} counter_state;
	volatile enum {
		STAGE_INPUT,
		STAGE_MUTE
	} set_stage;
	input_et active;
	mute_et mute;
} input_selector = { 0 };

void drv_input_init(void)
{
	hal_input_init();
	hal_mute_init();
	input_selector.active = INPUT_NONE;
	input_selector.mute = MUTE_ON;
}

void drv_input_set(input_et input, mute_et mute)
{
	hal_mute_on();

	input_selector.counter_state = COUNTER_STOPPED;
	input_selector.active = input;
	input_selector.mute = mute;
	input_selector.time_counter = RELAY_BOUNCE_TIME_MS;
	input_selector.set_stage = STAGE_INPUT;
	input_selector.counter_state = COUNTER_COUNTING;
}

/*
 * odlicza jednostki czasu
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void drv_input_on_tick_time(void)
{
	if (COUNTER_COUNTING == input_selector.counter_state) {
		if (0U < input_selector.time_counter) {
			input_selector.time_counter--;
		}

		if (0U == input_selector.time_counter) {
			switch (input_selector.set_stage) {
			case STAGE_INPUT:
				hal_input_off(INPUT_0);
				hal_input_off(INPUT_1);
				hal_input_off(INPUT_2);
				hal_input_off(INPUT_3);

				if (INPUT_ALL > input_selector.active) {
					hal_input_on(input_selector.active);
				} else if (INPUT_ALL == input_selector.active) {
					hal_input_on(INPUT_0);
					hal_input_on(INPUT_1);
					hal_input_on(INPUT_2);
					hal_input_on(INPUT_3);
				} else {
					;
				}

				input_selector.time_counter = RELAY_BOUNCE_TIME_MS;
				input_selector.set_stage = STAGE_MUTE;
				break;

			case STAGE_MUTE:
				if (MUTE_OFF == input_selector.mute) {
					hal_mute_off();
				}

				input_selector.counter_state = COUNTER_STOPPED;
				break;

			default:
				;
				break;
			}
		}
	}
}

