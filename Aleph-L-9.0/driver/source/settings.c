#include "../settings.h"

#include <avr/eemem.h>
#include <avr/io.h>
#include <common/crc8.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct {
	uint8_t sentinel;
	uint8_t crc8;
	settings_st settings;
} entry_st;

static entry_st entry;

static uint8_t const sentinel_mask = 1U << (sizeof(uint8_t) * 8U - 1U);
static uint16_t const entry_max = (E2END + 1U) / sizeof(entry_st);

static uintptr_t find_entry_address(void);
static void entry_read(void);
static void entry_write(void);

void drv_settings_init(void)
{
	/* ustawia początkową wartość strażnika w zmiennej entry */
	entry_read();
}

bool drv_settings_read(settings_st *settings)
{
	entry_read();

	bool settings_is_valid = crc8_is_match((uint8_t *) (&(entry.settings)), sizeof(settings_st), entry.crc8);

	if (true == settings_is_valid) {
		*settings = entry.settings;
	}

	return (settings_is_valid);
}

void drv_settings_write(settings_st *settings)
{
	entry.crc8 = crc8_generate((uint8_t *) settings, sizeof(settings_st));
	entry.settings = *settings;

	entry_write();
}

/* po resecie wszystkie komórki EEPROM mają wartości 0xFFU */
uintptr_t find_entry_address(void)
{
	entry_st e;

	avr_eemem_read(&e, (uintptr_t*) 0U, sizeof(e));

	uint8_t s = e.sentinel & sentinel_mask;

	/* wyszukiwanie binarne */
	uintptr_t left = 0U;
	uintptr_t right = entry_max;

	while ((left + 1U) != right) {
		uintptr_t mid = left + (right - left) / 2U;
		avr_eemem_read(&e, (uintptr_t*) (mid * sizeof(e)), sizeof(e));

		(s != (e.sentinel & sentinel_mask)) ? (right = mid) : (left = mid);
	}

	return (left * sizeof(e));
}

void entry_read(void)
{
	uintptr_t entry_address = find_entry_address();
	avr_eemem_read(&entry, (uintptr_t*) entry_address, sizeof(entry));
}

void entry_write(void)
{
	uintptr_t write_address = find_entry_address() + sizeof(entry);

	if ((write_address + sizeof(entry)) > (E2END + 1U)) {
		write_address = 0U;
		entry.sentinel ^= sentinel_mask;
	}

	avr_eemem_write(&entry, (uintptr_t*) write_address, sizeof(entry));
}

