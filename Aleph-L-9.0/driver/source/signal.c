#include "../../hal/signal.h"
#include "../signal.h"

#include <stdbool.h>
#include <stdint.h>

static struct {
	uint16_t on_time;
	uint16_t off_time;
	volatile uint16_t time_counter;
	volatile enum {
		COUNTER_STOPPED,
		COUNTER_COUNTING
	} counter_state;
	signal_et active;
} signal_indicator = { 0 };

static void signal_indicator_switch_state(void);

void drv_signal_indicator_init(void)
{
	hal_signal_init();
	signal_indicator.active = SIGNAL_ALL;
}

void drv_signal_indicator_on(signal_et signal)
{
	signal_indicator.counter_state = COUNTER_STOPPED;
	signal_indicator.active = signal;
	hal_signal_off(SIGNAL_ALL);
	hal_signal_on(signal_indicator.active);
}

void drv_signal_indicator_off(void)
{
	signal_indicator.counter_state = COUNTER_STOPPED;
	hal_signal_off(SIGNAL_ALL);
}

void drv_signal_indicator_blink(uint16_t on_time_ms, uint16_t off_time_ms)
{
	signal_indicator.on_time = on_time_ms;
	signal_indicator.off_time = off_time_ms;
	signal_indicator_switch_state();
	signal_indicator.counter_state = COUNTER_COUNTING;
}

void drv_signal_indicator_on_tick_time(void)
{
	if (COUNTER_COUNTING == signal_indicator.counter_state) {

		if (0U < signal_indicator.time_counter) {
			signal_indicator.time_counter--;
		}

		if (0U == signal_indicator.time_counter) {
			signal_indicator_switch_state();
		}
	}
}

void signal_indicator_switch_state(void)
{
	hal_signal_toggle(signal_indicator.active);
	signal_indicator.time_counter = (true == hal_signal_is_on(signal_indicator.active)) ?
			signal_indicator.on_time : signal_indicator.off_time;
}

