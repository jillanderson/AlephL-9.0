#include "../../hal/supply.h"
#include "../supply.h"

void drv_supply_init(void)
{
	hal_supply_init();
}

void drv_supply_on(void)
{
	hal_supply_on();
}

void drv_supply_off(void)
{
	hal_supply_off();
}

supply_status_et drv_supply_is_on(void)
{
	return (hal_supply_is_on());
}

