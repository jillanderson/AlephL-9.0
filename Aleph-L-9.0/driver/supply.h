#pragma once

/** \file **********************************************************************
 *
 * sterowanie zasilaniem
 *
 ******************************************************************************/

#include "../hal/supply.h"

#include <stdbool.h>

typedef enum {
	SUPPLY_OFF = false,
	SUPPLY_ON = true
} supply_status_et;

/*
 * inicjuje sterowanie zasilaniem
 */
void drv_supply_init(void);

/*
 * włącza zasilanie urządzenia
 */
void drv_supply_on(void);

/*
 * wyłącza zasilanie urządzenia
 */
void drv_supply_off(void);

/*
 * sprawdza czy zasilanie jest włączone
 *
 * @ret		SUPPLY_ON - zasilanie włączone
 *		SUPPLY_OFF - zasilanie wyłączone
 */
supply_status_et drv_supply_is_on(void);

