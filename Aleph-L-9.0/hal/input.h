#pragma once

/** \file **********************************************************************
 *
 * sterowanie przekaźnikami linii wejściowych
 *
 ******************************************************************************/

#include <stdbool.h>

typedef enum {
	INPUT_0,
	INPUT_1,
	INPUT_2,
	INPUT_3,
	INPUT_QTY,
	INPUT_ALL = INPUT_QTY,
	INPUT_NONE
} input_et;

/*
 * inicjuje sterowanie wejściami sygnału
 * wyłącza wszystkie wejścia
 */
void hal_input_init(void);

/*
 * włącza zadaną linię wejściową
 *
 * @input	linia wejściowa
 */
void hal_input_on(input_et input);

/*
 * wyłącza zadaną linię wejściową
 *
 * @input	linia wejściowa
 */
void hal_input_off(input_et input);

/*
 * przełącza zadaną linię wejściową w stan przeciwny
 *
 * @input	linia wejściowa
 */
void hal_input_toggle(input_et input);

/*
 * sprawdza czy linia wejściowa jest włączona
 *
 * @ret		true - linia włączona
 *		false - linia wyłączona
 */
bool hal_input_is_on(input_et input);

