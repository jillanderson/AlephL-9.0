#pragma once

/** \file **********************************************************************
 *
 * sprawdzenie stanu przycisku
 *
 * przycisk w stanie aktywnym (wciśnięty) zwiera pin do masy
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjuje obsługę przycisku
 */
void hal_key_init(void);

/*
 * sprawdza czy przycisk jest wciśnięty
 *
 * @ret		true - przycisk wciśnięty
 *		false - przycisk zwolniony
 */
bool hal_key_is_pressed(void);

