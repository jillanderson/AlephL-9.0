#pragma once

/** \file **********************************************************************
 *
 * sterowanie przekaźnikiem wyciszania sygnału
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjuje sterowanie wyciszaniem sygnału
 * wyciszenie włączone
 */
void hal_mute_init(void);

/*
 * włącza wyciszenie sygnału
 */
void hal_mute_on(void);

/*
 * wyłącza wyciszenie sygnału
 */
void hal_mute_off(void);

/*
 * przełącza wyciszenie sygnału w stan przeciwny
 */
void hal_mute_toggle(void);

/*
 * sprawdza czy wyciszenie sygnału jest włączone
 *
 * @ret		true - wyciszenie włączone
 *		false - wyciszenie wyłączone
 */
bool hal_mute_is_on(void);

