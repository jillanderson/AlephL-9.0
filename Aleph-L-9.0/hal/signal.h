#pragma once

/** \file **********************************************************************
 *
 * sterowanie wskaźnikami optycznymi linii wejściowych
 *
 ******************************************************************************/

#include <stdbool.h>

typedef enum {
	SIGNAL_0,
	SIGNAL_1,
	SIGNAL_2,
	SIGNAL_3,
	SIGNAL_QTY,
	SIGNAL_ALL = SIGNAL_QTY
} signal_et;

/*
 * inicjuje sterowanie wskaźnikami linii wejściowych
 * wyłącza wszystkie wskaźniki
 */
void hal_signal_init(void);

/*
 * włącza zadany wskaźnik linii wejściowej
 *
 * @signal	wskaźnik
 */
void hal_signal_on(signal_et signal);

/*
 * wyłącza zadany wskaźnik linii wejściowej
 *
 * @signal	wskaźnik
 */
void hal_signal_off(signal_et signal);

/*
 * przełącza zadany wskaźnik linii wejściowej w stan przeciwny
 *
 * @signal	wskaźnik
 */
void hal_signal_toggle(signal_et signal);

/*
 * sprawdza czy wskaźnik linii wejściowej jest włączony
 *
 * @ret		true - wskaźnik włączony
 *		false - wskaźnik wyłączony
 */
bool hal_signal_is_on(signal_et signal);

