#include "../attenuator.h"

#include <avr/avr_gpio.h>
#include <avr/io.h>
#include <stdbool.h>

static avr_gpio_pin_st const RELAY_ATTENUATOR_1DB = (avr_gpio_pin_st ) { .port = &PORTA, .pin_number = PA1 };
static avr_gpio_pin_st const RELAY_ATTENUATOR_2DB = (avr_gpio_pin_st ) { .port = &PORTA, .pin_number = PA2 };
static avr_gpio_pin_st const RELAY_ATTENUATOR_4DB = (avr_gpio_pin_st ) { .port = &PORTA, .pin_number = PA3 };
static avr_gpio_pin_st const RELAY_ATTENUATOR_8DB = (avr_gpio_pin_st ) { .port = &PORTA, .pin_number = PA4 };
static avr_gpio_pin_st const RELAY_ATTENUATOR_16DB = (avr_gpio_pin_st ) { .port = &PORTA, .pin_number = PA5 };
static avr_gpio_pin_st const RELAY_ATTENUATOR_32DB = (avr_gpio_pin_st ) { .port = &PORTA, .pin_number = PA6 };

void hal_attenuator_init(void)
{
	avr_gpio_configure_as_output(&RELAY_ATTENUATOR_32DB);
	avr_gpio_configure_as_output(&RELAY_ATTENUATOR_16DB);
	avr_gpio_configure_as_output(&RELAY_ATTENUATOR_8DB);
	avr_gpio_configure_as_output(&RELAY_ATTENUATOR_4DB);
	avr_gpio_configure_as_output(&RELAY_ATTENUATOR_2DB);
	avr_gpio_configure_as_output(&RELAY_ATTENUATOR_1DB);
	avr_gpio_set_high(&RELAY_ATTENUATOR_32DB);
	avr_gpio_set_high(&RELAY_ATTENUATOR_16DB);
	avr_gpio_set_high(&RELAY_ATTENUATOR_8DB);
	avr_gpio_set_high(&RELAY_ATTENUATOR_4DB);
	avr_gpio_set_high(&RELAY_ATTENUATOR_2DB);
	avr_gpio_set_high(&RELAY_ATTENUATOR_1DB);
}

void hal_attenuator_on(attenuator_et attenuator)
{
	switch (attenuator) {
	case ATTENUATOR_1DB:
		avr_gpio_set_high(&RELAY_ATTENUATOR_1DB);
		break;

	case ATTENUATOR_2DB:
		avr_gpio_set_high(&RELAY_ATTENUATOR_2DB);
		break;

	case ATTENUATOR_4DB:
		avr_gpio_set_high(&RELAY_ATTENUATOR_4DB);
		break;

	case ATTENUATOR_8DB:
		avr_gpio_set_high(&RELAY_ATTENUATOR_8DB);
		break;

	case ATTENUATOR_16DB:
		avr_gpio_set_high(&RELAY_ATTENUATOR_16DB);
		break;

	case ATTENUATOR_32DB:
		avr_gpio_set_high(&RELAY_ATTENUATOR_32DB);
		break;

	default:
		;
		break;
	}
}

void hal_attenuator_off(attenuator_et attenuator)
{
	switch (attenuator) {
	case ATTENUATOR_1DB:
		avr_gpio_set_low(&RELAY_ATTENUATOR_1DB);
		break;

	case ATTENUATOR_2DB:
		avr_gpio_set_low(&RELAY_ATTENUATOR_2DB);
		break;

	case ATTENUATOR_4DB:
		avr_gpio_set_low(&RELAY_ATTENUATOR_4DB);
		break;

	case ATTENUATOR_8DB:
		avr_gpio_set_low(&RELAY_ATTENUATOR_8DB);
		break;

	case ATTENUATOR_16DB:
		avr_gpio_set_low(&RELAY_ATTENUATOR_16DB);
		break;

	case ATTENUATOR_32DB:
		avr_gpio_set_low(&RELAY_ATTENUATOR_32DB);
		break;

	default:
		;
		break;
	}
}

void hal_attenuator_toggle(attenuator_et attenuator)
{
	switch (attenuator) {
	case ATTENUATOR_1DB:
		avr_gpio_set_opposite(&RELAY_ATTENUATOR_1DB);
		break;

	case ATTENUATOR_2DB:
		avr_gpio_set_opposite(&RELAY_ATTENUATOR_2DB);
		break;

	case ATTENUATOR_4DB:
		avr_gpio_set_opposite(&RELAY_ATTENUATOR_4DB);
		break;

	case ATTENUATOR_8DB:
		avr_gpio_set_opposite(&RELAY_ATTENUATOR_8DB);
		break;

	case ATTENUATOR_16DB:
		avr_gpio_set_opposite(&RELAY_ATTENUATOR_16DB);
		break;

	case ATTENUATOR_32DB:
		avr_gpio_set_opposite(&RELAY_ATTENUATOR_32DB);
		break;

	default:
		;
		break;
	}
}

bool hal_attenuator_is_on(attenuator_et attenuator)
{
	bool tmp = false;

	switch (attenuator) {
	case ATTENUATOR_1DB:
		if (0U != avr_gpio_read_pin(&RELAY_ATTENUATOR_1DB)) {
			tmp = true;
		}
		break;

	case ATTENUATOR_2DB:
		if (0U != avr_gpio_read_pin(&RELAY_ATTENUATOR_2DB)) {
			tmp = true;
		}
		break;

	case ATTENUATOR_4DB:
		if (0U != avr_gpio_read_pin(&RELAY_ATTENUATOR_4DB)) {
			tmp = true;
		}
		break;

	case ATTENUATOR_8DB:
		if (0U != avr_gpio_read_pin(&RELAY_ATTENUATOR_8DB)) {
			tmp = true;
		}
		break;

	case ATTENUATOR_16DB:
		if (0U != avr_gpio_read_pin(&RELAY_ATTENUATOR_16DB)) {
			tmp = true;
		}
		break;

	case ATTENUATOR_32DB:
		if (0U != avr_gpio_read_pin(&RELAY_ATTENUATOR_32DB)) {
			tmp = true;
		}
		break;

	default:
		;
		break;
	}

	return (tmp);
}

