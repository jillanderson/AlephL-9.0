#include "../encoder.h"

#include <avr/avr_gpio.h>
#include <avr/io.h>
#include <common/gray_code.h>
#include <stdint.h>

static avr_gpio_pin_st const ENCODER_0_CHANNEL_A = (avr_gpio_pin_st ) { .port = &PORTD, .pin_number = PD0 };
static avr_gpio_pin_st const ENCODER_0_CHANNEL_B = (avr_gpio_pin_st ) { .port = &PORTD, .pin_number = PD1 };
static avr_gpio_pin_st const ENCODER_1_CHANNEL_A = (avr_gpio_pin_st ) { .port = &PORTD, .pin_number = PD4 };
static avr_gpio_pin_st const ENCODER_1_CHANNEL_B = (avr_gpio_pin_st ) { .port = &PORTD, .pin_number = PD5 };

void hal_encoder_init(void)
{
	avr_gpio_configure_as_input(&ENCODER_0_CHANNEL_A);
	avr_gpio_configure_as_input(&ENCODER_0_CHANNEL_B);
	avr_gpio_configure_as_input(&ENCODER_1_CHANNEL_A);
	avr_gpio_configure_as_input(&ENCODER_1_CHANNEL_B);
	avr_gpio_set_low(&ENCODER_0_CHANNEL_A);
	avr_gpio_set_low(&ENCODER_0_CHANNEL_B);
	avr_gpio_set_low(&ENCODER_1_CHANNEL_A);
	avr_gpio_set_low(&ENCODER_1_CHANNEL_B);
}

uint8_t hal_encoder_read_state(encoder_et encoder)
{
	uint8_t data = 0U;
	uint8_t clk = 0U;

	switch (encoder) {
	case ENCODER_INPUT:
		data = avr_gpio_read_pin(&ENCODER_0_CHANNEL_A);
		clk = avr_gpio_read_pin(&ENCODER_0_CHANNEL_B);
		break;

	case ENCODER_VOLUME:
		data = avr_gpio_read_pin(&ENCODER_1_CHANNEL_A);
		clk = avr_gpio_read_pin(&ENCODER_1_CHANNEL_B);
		break;

	default:
		;
		break;
	}

	uint8_t code = gray_code(data, clk);

	return (code);
}

