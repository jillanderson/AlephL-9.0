#include "../input.h"

#include <avr/avr_gpio.h>
#include <avr/io.h>
#include <stdbool.h>

static avr_gpio_pin_st const RELAY_INPUT_0 = (avr_gpio_pin_st ) { .port = &PORTB, .pin_number = PB0 };
static avr_gpio_pin_st const RELAY_INPUT_1 = (avr_gpio_pin_st ) { .port = &PORTB, .pin_number = PB1 };
static avr_gpio_pin_st const RELAY_INPUT_2 = (avr_gpio_pin_st ) { .port = &PORTB, .pin_number = PB2 };
static avr_gpio_pin_st const RELAY_INPUT_3 = (avr_gpio_pin_st ) { .port = &PORTB, .pin_number = PB3 };

void hal_input_init(void)
{
	avr_gpio_configure_as_output(&RELAY_INPUT_0);
	avr_gpio_configure_as_output(&RELAY_INPUT_1);
	avr_gpio_configure_as_output(&RELAY_INPUT_2);
	avr_gpio_configure_as_output(&RELAY_INPUT_3);
	avr_gpio_set_low(&RELAY_INPUT_0);
	avr_gpio_set_low(&RELAY_INPUT_1);
	avr_gpio_set_low(&RELAY_INPUT_2);
	avr_gpio_set_low(&RELAY_INPUT_3);
}

void hal_input_on(input_et input)
{
	switch (input) {
	case INPUT_0:
		avr_gpio_set_high(&RELAY_INPUT_0);
		break;

	case INPUT_1:
		avr_gpio_set_high(&RELAY_INPUT_1);
		break;

	case INPUT_2:
		avr_gpio_set_high(&RELAY_INPUT_2);
		break;

	case INPUT_3:
		avr_gpio_set_high(&RELAY_INPUT_3);
		break;

	default:
		;
		break;
	}
}

void hal_input_off(input_et input)
{
	switch (input) {
	case INPUT_0:
		avr_gpio_set_low(&RELAY_INPUT_0);
		break;

	case INPUT_1:
		avr_gpio_set_low(&RELAY_INPUT_1);
		break;

	case INPUT_2:
		avr_gpio_set_low(&RELAY_INPUT_2);
		break;

	case INPUT_3:
		avr_gpio_set_low(&RELAY_INPUT_3);
		break;

	default:
		;
		break;
	}
}

void hal_input_toggle(input_et input)
{
	switch (input) {
	case INPUT_0:
		avr_gpio_set_opposite(&RELAY_INPUT_0);
		break;

	case INPUT_1:
		avr_gpio_set_opposite(&RELAY_INPUT_1);
		break;

	case INPUT_2:
		avr_gpio_set_opposite(&RELAY_INPUT_2);
		break;

	case INPUT_3:
		avr_gpio_set_opposite(&RELAY_INPUT_3);
		break;

	default:
		;
		break;
	}
}

bool hal_input_is_on(input_et input)
{
	bool tmp = false;

	switch (input) {
	case INPUT_0:
		if (0U != avr_gpio_read_pin(&RELAY_INPUT_0)) {
			tmp = true;
		}
		break;

	case INPUT_1:
		if (0U != avr_gpio_read_pin(&RELAY_INPUT_1)) {
			tmp = true;
		}
		break;

	case INPUT_2:
		if (0U != avr_gpio_read_pin(&RELAY_INPUT_2)) {
			tmp = true;
		}
		break;

	case INPUT_3:
		if (0U != avr_gpio_read_pin(&RELAY_INPUT_3)) {
			tmp = true;
		}
		break;

	default:
		;
		break;
	}

	return (tmp);
}

