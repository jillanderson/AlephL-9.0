#include "../signal.h"

#include <avr/avr_gpio.h>
#include <avr/io.h>
#include <stdbool.h>

static avr_gpio_pin_st const LED_SIGNAL_0 = (avr_gpio_pin_st ) { .port = &PORTC, .pin_number = PC4 };
static avr_gpio_pin_st const LED_SIGNAL_1 = (avr_gpio_pin_st ) { .port = &PORTC, .pin_number = PC5 };
static avr_gpio_pin_st const LED_SIGNAL_2 = (avr_gpio_pin_st ) { .port = &PORTC, .pin_number = PC6 };
static avr_gpio_pin_st const LED_SIGNAL_3 = (avr_gpio_pin_st ) { .port = &PORTC, .pin_number = PC7 };

void hal_signal_init(void)
{
	avr_gpio_configure_as_output(&LED_SIGNAL_0);
	avr_gpio_configure_as_output(&LED_SIGNAL_1);
	avr_gpio_configure_as_output(&LED_SIGNAL_2);
	avr_gpio_configure_as_output(&LED_SIGNAL_3);
	avr_gpio_set_low(&LED_SIGNAL_0);
	avr_gpio_set_low(&LED_SIGNAL_1);
	avr_gpio_set_low(&LED_SIGNAL_2);
	avr_gpio_set_low(&LED_SIGNAL_3);
}

void hal_signal_on(signal_et signal)
{
	switch (signal) {
	case SIGNAL_0:
		avr_gpio_set_high(&LED_SIGNAL_0);
		break;

	case SIGNAL_1:
		avr_gpio_set_high(&LED_SIGNAL_1);
		break;

	case SIGNAL_2:
		avr_gpio_set_high(&LED_SIGNAL_2);
		break;

	case SIGNAL_3:
		avr_gpio_set_high(&LED_SIGNAL_3);
		break;

	case SIGNAL_ALL:
		avr_gpio_set_high(&LED_SIGNAL_0);
		avr_gpio_set_high(&LED_SIGNAL_1);
		avr_gpio_set_high(&LED_SIGNAL_2);
		avr_gpio_set_high(&LED_SIGNAL_3);
		break;

	default:
		;
		break;
	}
}

void hal_signal_off(signal_et signal)
{
	switch (signal) {
	case SIGNAL_0:
		avr_gpio_set_low(&LED_SIGNAL_0);
		break;

	case SIGNAL_1:
		avr_gpio_set_low(&LED_SIGNAL_1);
		break;

	case SIGNAL_2:
		avr_gpio_set_low(&LED_SIGNAL_2);
		break;

	case SIGNAL_3:
		avr_gpio_set_low(&LED_SIGNAL_3);
		break;

	case SIGNAL_ALL:
		avr_gpio_set_low(&LED_SIGNAL_0);
		avr_gpio_set_low(&LED_SIGNAL_1);
		avr_gpio_set_low(&LED_SIGNAL_2);
		avr_gpio_set_low(&LED_SIGNAL_3);
		break;

	default:
		;
		break;
	}
}

void hal_signal_toggle(signal_et signal)
{
	switch (signal) {
	case SIGNAL_0:
		avr_gpio_set_opposite(&LED_SIGNAL_0);
		break;

	case SIGNAL_1:
		avr_gpio_set_opposite(&LED_SIGNAL_1);
		break;

	case SIGNAL_2:
		avr_gpio_set_opposite(&LED_SIGNAL_2);
		break;

	case SIGNAL_3:
		avr_gpio_set_opposite(&LED_SIGNAL_3);
		break;

	case SIGNAL_ALL:
		avr_gpio_set_opposite(&LED_SIGNAL_0);
		avr_gpio_set_opposite(&LED_SIGNAL_1);
		avr_gpio_set_opposite(&LED_SIGNAL_2);
		avr_gpio_set_opposite(&LED_SIGNAL_3);
		break;

	default:
		;
		break;
	}
}

bool hal_signal_is_on(signal_et signal)
{
	bool tmp = false;

	switch (signal) {
	case SIGNAL_0:
		if (0U != avr_gpio_read_pin(&LED_SIGNAL_0)) {
			tmp = true;
		}
		break;

	case SIGNAL_1:
		if (0U != avr_gpio_read_pin(&LED_SIGNAL_1)) {
			tmp = true;
		}
		break;

	case SIGNAL_2:
		if (0U != avr_gpio_read_pin(&LED_SIGNAL_2)) {
			tmp = true;
		}
		break;

	case SIGNAL_3:
		if (0U != avr_gpio_read_pin(&LED_SIGNAL_3)) {
			tmp = true;
		}
		break;

	case SIGNAL_ALL:
		if ((0U != avr_gpio_read_pin(&LED_SIGNAL_0))
				&& (0U != avr_gpio_read_pin(&LED_SIGNAL_1))
				&& (0U != avr_gpio_read_pin(&LED_SIGNAL_2))
				&& (0U != avr_gpio_read_pin(&LED_SIGNAL_3))) {
			tmp = true;
		}
		break;

	default:
		;
		break;
	}

	return (tmp);
}

