#include "../supply.h"

#include <avr/avr_gpio.h>
#include <avr/io.h>
#include <stdbool.h>

static avr_gpio_pin_st const RELAY_SUPPLY = (avr_gpio_pin_st ) { .port = &PORTA, .pin_number = PA7 };

void hal_supply_init(void)
{
	avr_gpio_configure_as_output(&RELAY_SUPPLY);
	avr_gpio_set_low(&RELAY_SUPPLY);
}

void hal_supply_on(void)
{
	avr_gpio_set_high(&RELAY_SUPPLY);
}

void hal_supply_off(void)
{
	avr_gpio_set_low(&RELAY_SUPPLY);
}

bool hal_supply_is_on(void)
{
	return (0U == avr_gpio_read_pin(&RELAY_SUPPLY) ? false : true);
}

