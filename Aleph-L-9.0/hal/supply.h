#pragma once

/** \file **********************************************************************
 *
 * sterowanie przekaźnikiem kontrolującym zasilanie
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjuje sterowanie zasilaniem
 */
void hal_supply_init(void);

/*
 * włącza zasilanie
 */
void hal_supply_on(void);

/*
 * wyłącza zasilanie
 */
void hal_supply_off(void);

/*
 * sprawdza czy zasilanie jest włączone
 *
 * @ret		true - wskaźnik włączony
 *		false - wskaźnik wyłączony
 */
bool hal_supply_is_on(void);

